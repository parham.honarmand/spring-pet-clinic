package com.parham.springpetclinic.service;

import com.parham.springpetclinic.model.Vet;

public interface VetService extends Service<Vet, Long> {
}
