package com.parham.springpetclinic.service;

import com.parham.springpetclinic.model.Pet;

public interface PetService extends Service<Pet, Long> {
}
