package com.parham.springpetclinic.service;

import java.util.Set;

public interface Service<T, ID> {
    T findById(ID id);

    T save(T owner);

    Set<T> findAll();
}
