package com.parham.springpetclinic.service;

import com.parham.springpetclinic.model.Owner;

public interface OwnerService extends Service<Owner, Long> {
    Owner findByLastName(String lastName);
}
